### (WIP) Test service for Gymshark 
#### λ Run
Локально
```shell script
go run cmd/api/main.go
```
Docker
```shell script
docker build -t gymshark .
docker run -d -p 4000:4000 --name gymshark gymshark
```
#### λ Хелзчек
Health check `/api/v1/`:
```json
{"message":"OK","status":true}
```

Start message
```text
Gymshark services v1.0.0
```
Launched on the port

```text
Port: 4000
```

#### All configurations

#### APP
```dotenv
VERSION=1.0.0
PORT=4000
```
#### DB
```dotenv
DB_HOST=
DB_PORT=
DB_NAME=
DB_USER=
DB_PASSWORD=
DB_LOG=1
DB_MAX_OPEN_CONN=10
```

#### λ For the swagger
```text
swag init --parseDependency  --parseInternal --parseDepth 1  -g ./cmd/api/main.go
```
