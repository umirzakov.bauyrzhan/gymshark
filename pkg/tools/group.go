package tools

import (
	"fmt"
)

func Group(s []float64) string {
	duplicate := make(map[float64]int)
	for _, item := range s {
		_, exist := duplicate[item]
		if exist {
			duplicate[item] += 1
		} else {
			duplicate[item] = 1
		}
	}

	var res string
	for k, v := range duplicate {
		res += fmt.Sprintf(` %d x %v;`, v, k)
	}

	return res
}
