package app

import (
	fiberSwagger "github.com/arsmn/fiber-swagger/v2"
	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/cors"
	"github.com/gofiber/fiber/v2/middleware/logger"
	"github.com/joho/godotenv"
	_ "gymshark/docs"
	v1 "gymshark/internal/api/v1"
	"gymshark/internal/api/v1/handlers"
	"gymshark/internal/repository"
	"gymshark/internal/service"
	"gymshark/pkg/db/postgres"
	"log"
	"os"
	"os/signal"
	"syscall"
	"time"
)

const idleTimeout = 10 * time.Second

func Run() {
	if e := godotenv.Load(); e != nil {
		log.Println(".env variables load err: ", e.Error())
	}
	log.Printf("Gymshark services v%s\n", os.Getenv("VERSION"))
	_port := os.Getenv("PORT")

	// If we need the DB, but in this test I don't use the DB this for just example
	_db := postgres.NewDB(&postgres.DBConfig{
		Host:     os.Getenv("DB_HOST"),
		Port:     os.Getenv("DB_PORT"),
		User:     os.Getenv("DB_USER"),
		Password: os.Getenv("DB_PASSWORD"),
		Name:     os.Getenv("DB_NAME"),
	})
	_repos := repository.NewRepositories(_db)

	_services := service.NewServices(service.Dependencies{
		Repos: _repos,
	})

	_handlers := handlers.NewHeandler(_services)

	_app := fiber.New(fiber.Config{
		IdleTimeout: idleTimeout,
		ErrorHandler: func(ctx *fiber.Ctx, err error) error {
			return ctx.Status(fiber.StatusInternalServerError).JSON(fiber.Map{
				"status":  false,
				"message": err.Error(),
			})
		},
	})

	_app.Use(logger.New(), cors.New(cors.Config{
		AllowOrigins:     "http://localhost:3000,http://localhost:3001",
		AllowCredentials: true,
		AllowHeaders:     "Origin, Content-Type, Accept, Authorization",
	}))

	_app.Get("/swagger/*", fiberSwagger.New(fiberSwagger.Config{
		DeepLinking: false,
		URL:         "/swagger/doc.json",
	}))

	v1.Routes(_app, _handlers)

	go func() {
		if err := _app.Listen(":" + _port); err != nil {
			log.Panic(err)
		}
	}()

	c := make(chan os.Signal, 1)                    // Create channel to signify a signal being sent
	signal.Notify(c, os.Interrupt, syscall.SIGTERM) // When an interrupt or termination signal is sent, notify the channel

	_ = <-c // This blocks the main thread until an interrupt is received
	log.Println("Gracefully shutting down...")
	_ = _app.Shutdown()

	log.Println("Running cleanup tasks...")

	// Your cleanup tasks go here
	_db.Close()

	log.Println("Monitoring service was successful shutdown.")

}
