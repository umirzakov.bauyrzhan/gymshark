package repository

import (
	"github.com/jmoiron/sqlx"
)

type Ships interface {
	//Here you should do realization methods on the DataBase
}

type Repositories struct {
	Ships Ships
}

func NewRepositories(db *sqlx.DB) *Repositories {
	var shipsRepo = NewShipsDB(db)
	return &Repositories{shipsRepo}
}
