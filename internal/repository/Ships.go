package repository

import "github.com/jmoiron/sqlx"

type ShipsDB struct {
	db *sqlx.DB
}

func NewShipsDB(db *sqlx.DB) *ShipsDB {
	return &ShipsDB{db: db}
}
