package models

type HTTPSuccess struct {
	Status  bool         `json:"status"`
	Message string       `json:"message"`
	Result  *interface{} `json:"result"`
}

type HTTPError struct {
	Status  bool   `json:"status"`
	Message string `json:"message"`
}

type ClaimsData struct {
	UserId *string `json:"user_id"`
}
