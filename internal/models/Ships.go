package models

type OrderReq struct {
	CompanyId   string  `json:"company_id" example:"e340aadd-83fc-4923-b9bc-be0174c8f6c8"`
	ItemsNumber float64 `json:"items_number" example:"12001"`
	PackSize1   float64 `json:"pack_size_1" example:"250"`
	PackSize2   float64 `json:"pack_size_2" example:"500"`
	PackSize3   float64 `json:"pack_size_3" example:"1000"`
	PackSize4   float64 `json:"pack_size_4" example:"2000"`
	PackSize5   float64 `json:"pack_size_5" example:"5000"`
}

type OrderRes struct {
	CompanyId string  `json:"company_id"`
	PackSize  float64 `json:"pack_size"`
	PackCount float64 `json:"pack_count"`
}
