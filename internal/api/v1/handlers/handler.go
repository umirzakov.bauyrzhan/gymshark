package handlers

import (
	"gymshark/internal/service"
)

type Handler struct {
	services *service.Services
}

func NewHeandler(services *service.Services) *Handler {
	return &Handler{services: services}
}
