package handlers

import (
	"fmt"
	"github.com/gofiber/fiber/v2"
	"gymshark/internal/models"
	"gymshark/internal/schema"
)

// Order
// @Summary Solve pack numbers
// @Accept json
// @Produce json
// @Param data body models.OrderReq true "Data for solve pack number"
// @Success 200 {object} schema.F[schema.Empty]
// @Failure 400 {object} schema.F[schema.Empty]
// @tags Gymshark
// @Router /v1/packs [post]
func (h *Handler) Order(c *fiber.Ctx) (err error) {
	var req models.OrderReq
	if err = c.BodyParser(&req); err != nil {
		return schema.ErrorRespond(&schema.Empty{}, c, fiber.StatusBadRequest, fmt.Sprintf(`Error in parsing req body: %s`, err.Error()))
	}
	r, err := h.services.Ships.Order(c.Context(), &req)
	if err != nil {
		return schema.ErrorRespond(&schema.Empty{}, c, fiber.StatusBadRequest, err.Error())
	}

	return schema.Respond(&r, c)
}
