package v1

import (
	"github.com/gofiber/fiber/v2"
	"gymshark/internal/api/v1/handlers"
	"gymshark/internal/models"
)

func Routes(app *fiber.App, h *handlers.Handler) {

	v1 := app.Group("/v1/")

	// Heath check
	v1.Get("/", func(c *fiber.Ctx) error {
		return c.JSON(models.HTTPSuccess{
			Status:  true,
			Message: "success",
		})
	})

	v1.Post("/packs", h.Order)
}
