package service

import (
	"context"
	"gymshark/internal/models"
	"gymshark/internal/repository"
)

type Ships interface {
	//Here you should do realization with the Direction entity
	Order(ctx context.Context, req *models.OrderReq) (res *string, err error)
}

type Services struct {
	Ships Ships
}

type Dependencies struct {
	Repos *repository.Repositories
}

func NewServices(deps Dependencies) *Services {
	shipsService := NewShipsService(deps.Repos)
	return &Services{shipsService}
}
