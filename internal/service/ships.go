package service

import (
	"context"
	"fmt"
	"gymshark/internal/models"
	"gymshark/internal/repository"
	"gymshark/pkg/tools"
)

type ShipsService struct {
	repos *repository.Repositories
}

func NewShipsService(repos *repository.Repositories) *ShipsService {
	return &ShipsService{repos: repos}
}

func (s ShipsService) Order(ctx context.Context, req *models.OrderReq) (res *string, err error) {
	var (
		packSize1 = req.PackSize1
		packSize2 = req.PackSize2
		packSize3 = req.PackSize3
		packSize4 = req.PackSize4
		packSize5 = req.PackSize5
		delta     float64
		packs     []float64
	)
	items := req.ItemsNumber

	for {
		delta = items / packSize1
		if delta <= 1 {
			packs = append(packs, packSize1)
			break
		} else {
			delta = items / packSize2
			if delta <= 1 {
				packs = append(packs, packSize2)
				break
			} else {
				delta = items / packSize3
				if delta <= 1 {
					packs = append(packs, packSize2)
					items = items - packSize2
					continue
				} else {
					delta = items / packSize4
					if delta <= 1 {
						packs = append(packs, packSize3)
						items = items - packSize3
						continue
					} else {
						delta = items / packSize5
						if delta <= 1 {
							packs = append(packs, packSize4)
							items = items - packSize4
							continue
						} else {
							packs = append(packs, packSize5)
							items = items - packSize5
							continue
						}
					}
				}
			}
		}
	}

	strPacks := tools.Group(packs)

	fmt.Println("strPacks")
	fmt.Println(strPacks)

	return &strPacks, nil
}
