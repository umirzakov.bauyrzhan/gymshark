package schema

import "github.com/gofiber/fiber/v2"

type Empty = struct{}

type F[T any] struct {
	Status  bool   `json:"status"`
	Message string `json:"message"`
	Result  *T     `json:"result,omitempty"`
}

func Respond[T any](v *T, c *fiber.Ctx) error {
	return c.Status(fiber.StatusOK).JSON(F[T]{
		Status:  true,
		Message: "Success",
		Result:  v,
	})
}

func ErrorRespond[T any](v *T, c *fiber.Ctx, code int, errorMessage string) error {
	return c.Status(code).JSON(F[T]{
		Status:  false,
		Message: errorMessage,
		Result:  v,
	})
}
