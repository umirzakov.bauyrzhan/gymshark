package main

import "gymshark/internal/app"

// @title Gymshark test project
// @version 1.0.0
// @description Swagger Documentation for Gymshark Project
// @host 34.30.132.28
// @BasePath /
func main() {
	app.Run()
}
