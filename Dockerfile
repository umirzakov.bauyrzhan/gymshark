# build stage
FROM golang:1.20-alpine AS builder
RUN mkdir gymshark
RUN chmod 777 -R ./gymshark
COPY . /gymshark
WORKDIR /gymshark
RUN go mod download
RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -o gymshark ./cmd/api/main.go

# final stage
FROM alpine:latest AS final
RUN mkdir app
RUN chmod 777 -R ./app
COPY --from=builder /gymshark /app
WORKDIR /app
RUN chmod +x ./gymshark
ENTRYPOINT ["./gymshark"]
EXPOSE 4000
